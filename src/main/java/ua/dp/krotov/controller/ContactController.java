package ua.dp.krotov.controller;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.datasources.ResponseData;
import ua.dp.krotov.model.Contact;
import ua.dp.krotov.service.ContactService;
import ua.dp.krotov.service.ResponseCreator;


import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Evgen on 13.09.2016.
 */
@RestController
@RequestMapping(value = "/hello")
public class ContactController {

    private static final Logger logger = Logger.getLogger(ContactController.class);

    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/contacts",
            method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseBody
    public Response getContacts(@RequestParam(value = "nameFilter") final String param,
                                @RequestParam(value = "rEcho", required = false)  Integer rEcho,
                                @RequestParam(value = "rDisplayStart", required = false)  Integer rDisplayStart,
                                @RequestParam(value = "rDisplayLength", required = false) Integer rDisplayLength) {
        logger.info("Request ContactController (RequestMapping = /hello/contacts) with param nameFilter = " + param);

        if (rEcho == null) rEcho = 0;
        if(rDisplayLength == null) rDisplayLength = 25;
        if(rDisplayStart == null) rDisplayStart = 0;
        ResponseData responseData = new ResponseData();
        List<Contact> list = contactService.get(param, rDisplayStart, rDisplayLength);

            responseData.setRData(list);
            responseData.setREcho(rEcho);
            responseData.setRTotalRecords(25);
            responseData.setRTotalDisplayRecords(60);

        return ResponseCreator.success(responseData);
    }

}
