package ua.dp.krotov.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Evgen on 13.09.2016.
 */
@Entity
@Table(name = "contact")
@Data
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
}
