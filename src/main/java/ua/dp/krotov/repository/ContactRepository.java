package ua.dp.krotov.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import ua.dp.krotov.model.Contact;

/**
 * Created by Evgen on 13.09.2016.
 */
public interface ContactRepository extends CrudRepository<Contact, Integer>, JpaSpecificationExecutor<Contact> {
}
