package ua.dp.krotov.datasources;

import lombok.Data;

import java.util.List;

/**
 * Created by Evgen on 13.09.2016.
 */
@Data
public class ResponseData<T> {
    private int rEcho;
    private int rTotalRecords;
    private int rTotalDisplayRecords;

    private List<T> rData;
}
