package ua.dp.krotov.service;

import ua.dp.krotov.model.Contact;

import java.util.List;
import java.util.Map;

/**
 * Created by Evgen on 13.09.2016.
 */

public interface ContactService {
    Map<Long, List<Contact>> get(String param, Integer rDisplayStart, Integer rDisplayLength);
}
