package ua.dp.krotov.service;

import javax.ws.rs.core.Response;

/**
 * Created by Evgen on 13.09.2016.
 */
public class ResponseCreator {

    public static Response error(int status, int errorCode, String version) {
        Response.ResponseBuilder response = Response.status(status);
        response.header("version", version);
        response.header("errorcode", errorCode);
        response.entity("none");
        return response.build();
    }

    public static Response success(Object object) {
        Response.ResponseBuilder response = Response.ok();
        if (object != null) {
            response.entity(object);
        } else {
            response.entity("none");
        }
        return response.build();
    }
}
