package ua.dp.krotov.service;

import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.engine.spi.TypedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Contact;
import ua.dp.krotov.repository.ContactRepository;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Evgen on 13.09.2016.
 */
@Service
public class ContactServiceImpl implements ContactService {
//
//    @Autowired
//    private ContactRepository contactRepository;

    private Pattern pattern;
    private Matcher matcher;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Map<Long, List<Contact>> get(String param, Integer rDisplayStart, Integer rDisplayLength) {

//        Specification specification = new Specification() {
//            @Override
//            public Predicate toPredicate(Root<Contact> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
//                return criteriaBuilder.not(root.get("name").in(pattern));
//            }
//        };
//        pattern = Pattern.compile(param);
//        Long totalLength = contactRepository.count(Specifications.not(FilterSpecification.findByPattern(pattern)));
//        Long totalLength2 = contactRepository.count(Specifications.where(FilterSpecification.findByPattern(pattern)));



//        pattern = Pattern.compile(param);

//        final PageRequest pageReq = new PageRequest( rDisplayStart / rDisplayLength, rDisplayLength);
//        Page<Contact> page = contactRepository.findAll(
//                Specifications.where(FilterSpecification.findByPattern(pattern))
//        ,
//                pageReq);


        pattern = Pattern.compile(param);
        Session session = sessionFactory.openSession();
//        Criteria criteria = session.createCriteria(Contact.class)
//                .add(Restrictions.ilike("name", "^t.*$"));
//        criteria.setMaxResults(rDisplayLength);
//        criteria.setFirstResult(rDisplayStart);


        List<Contact> contactList = new ArrayList<>();
        Map<Long, List<Contact>> longListMap = new HashMap<>();
        ScrollableResults results = session.createCriteria(Contact.class)
//                .setMaxResults(rDisplayLength)
                .setFetchSize(100)
                .setCacheMode(CacheMode.IGNORE)
                .setFirstResult(rDisplayStart)
                .scroll(ScrollMode.FORWARD_ONLY);

        while ( results.next() ) {
            Contact contact = (Contact) results.get(0);
            matcher = pattern.matcher(contact.getName());
            if (!matcher.find()) {
                contactList.add(contact);
                if(contactList.size() == rDisplayLength){
                    longListMap.put(contact.getId(), contactList);
                    session.close();
                    return longListMap;}
            }
        }

        longListMap.put((contactList.get(contactList.size()-1)).getId(), contactList);
        session.close();
        return longListMap;
    }
}
